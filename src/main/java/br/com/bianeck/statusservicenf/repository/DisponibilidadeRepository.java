package br.com.bianeck.statusservicenf.repository;

import br.com.bianeck.statusservicenf.controller.dto.CountDisponibilidadeDto;
import br.com.bianeck.statusservicenf.model.Disponibilidade;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

public interface DisponibilidadeRepository extends JpaRepository<Disponibilidade, Long> {

    Page<Disponibilidade> findByUf(String uf, Pageable paginacao);
    Page<Disponibilidade> findByDataConsultaBetween(
            @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") @Param("from") LocalDateTime from,
            @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") @Param("to") LocalDateTime to,
            Pageable paginacao);
    Page<Disponibilidade> findByUfAndDataConsultaBetween(
            String uf,
            @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") @Param("from") LocalDateTime from,
            @DateTimeFormat(pattern = "dd/MM/yyyy HH:mm:ss") @Param("to") LocalDateTime to,
            Pageable paginacao);

    Page<Disponibilidade> findByVersaoWebService(String versaoWebService, Pageable paginacao);

    @Query("SELECT d.uf as "+ CountDisponibilidadeDto.UF +", " +
            "d.versaoWebService as "+ CountDisponibilidadeDto.VERSAO_WEB_SERVICE +",\n" +
            "SUM((CASE WHEN d.statusAutorizacao = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusRetornoAutorizacao = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusInutilizacao = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusConsultaProtocolo = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusStatusServico = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusConsultaCadastro = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusRecepcaoEvento = TRUE THEN 1 ELSE 0 END)) AS " + CountDisponibilidadeDto.DISPONIBILIDADES + ",\n" +
            "SUM((CASE WHEN d.statusAutorizacao = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusRetornoAutorizacao = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusInutilizacao = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusConsultaProtocolo = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusStatusServico = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusConsultaCadastro = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusRecepcaoEvento = TRUE THEN 0 ELSE 1 END)) AS " + CountDisponibilidadeDto.INDISPONIBILIDADES + "\n" +
            " FROM Disponibilidade d \n" +
            " where d.versaoWebService = :version \n" +
            "GROUP BY d.uf, d.versaoWebService ORDER BY indisponibilidades ASC")
            List<Map<String, Object>> countDisponibilidadeByVersaoWebService(@Param("version") String version);

    @Query("SELECT d.uf as "+ CountDisponibilidadeDto.UF +", " +
            "d.versaoWebService as "+ CountDisponibilidadeDto.VERSAO_WEB_SERVICE +",\n" +
            "SUM((CASE WHEN d.statusAutorizacao = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusRetornoAutorizacao = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusInutilizacao = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusConsultaProtocolo = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusStatusServico = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusConsultaCadastro = TRUE THEN 1 ELSE 0 END) +\n" +
            "(CASE WHEN d.statusRecepcaoEvento = TRUE THEN 1 ELSE 0 END)) AS " + CountDisponibilidadeDto.DISPONIBILIDADES + ",\n" +
            "SUM((CASE WHEN d.statusAutorizacao = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusRetornoAutorizacao = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusInutilizacao = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusConsultaProtocolo = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusStatusServico = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusConsultaCadastro = TRUE THEN 0 ELSE 1 END) +\n" +
            "(CASE WHEN d.statusRecepcaoEvento = TRUE THEN 0 ELSE 1 END)) AS " + CountDisponibilidadeDto.INDISPONIBILIDADES + "\n" +
            " FROM Disponibilidade d \n" +
            "GROUP BY d.uf, d.versaoWebService ORDER BY indisponibilidades ASC")
    List<Map<String, Object>> countDisponibilidade();

}
