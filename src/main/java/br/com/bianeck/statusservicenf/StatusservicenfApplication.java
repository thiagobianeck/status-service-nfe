package br.com.bianeck.statusservicenf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.scheduling.annotation.EnableScheduling;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableScheduling
@EnableSpringDataWebSupport
@EnableSwagger2
public class StatusservicenfApplication {

	public static void main(String[] args) {
		SpringApplication.run(StatusservicenfApplication.class, args);
	}

}
