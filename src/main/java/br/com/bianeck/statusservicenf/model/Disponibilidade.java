package br.com.bianeck.statusservicenf.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
public class Disponibilidade {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String uf;
    private boolean statusAutorizacao;
    private boolean statusRetornoAutorizacao;
    private boolean statusInutilizacao;
    private boolean statusConsultaProtocolo;
    private boolean statusStatusServico;
    private boolean statusConsultaCadastro;
    private boolean statusRecepcaoEvento;
    private String versaoWebService;
    private LocalDateTime dataConsulta = LocalDateTime.now();

    public Disponibilidade() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public boolean isStatusAutorizacao() {
        return statusAutorizacao;
    }

    public void setStatusAutorizacao(boolean statusAutorizacao) {
        this.statusAutorizacao = statusAutorizacao;
    }

    public boolean isStatusRetornoAutorizacao() {
        return statusRetornoAutorizacao;
    }

    public void setStatusRetornoAutorizacao(boolean statusRetornoAutorizacao) {
        this.statusRetornoAutorizacao = statusRetornoAutorizacao;
    }

    public boolean isStatusInutilizacao() {
        return statusInutilizacao;
    }

    public void setStatusInutilizacao(boolean statusInutilizacao) {
        this.statusInutilizacao = statusInutilizacao;
    }

    public boolean isStatusConsultaProtocolo() {
        return statusConsultaProtocolo;
    }

    public void setStatusConsultaProtocolo(boolean statusConsultaProtocolo) {
        this.statusConsultaProtocolo = statusConsultaProtocolo;
    }

    public boolean isStatusStatusServico() {
        return statusStatusServico;
    }

    public void setStatusStatusServico(boolean statusStatusServico) {
        this.statusStatusServico = statusStatusServico;
    }

    public boolean isStatusConsultaCadastro() {
        return statusConsultaCadastro;
    }

    public void setStatusConsultaCadastro(boolean statusConsultaCadastro) {
        this.statusConsultaCadastro = statusConsultaCadastro;
    }

    public boolean isStatusRecepcaoEvento() {
        return statusRecepcaoEvento;
    }

    public void setStatusRecepcaoEvento(boolean statusRecepcaoEvento) {
        this.statusRecepcaoEvento = statusRecepcaoEvento;
    }

    public String getVersaoWebService() {
        return versaoWebService;
    }

    public void setVersaoWebService(String versaoWebService) {
        this.versaoWebService = versaoWebService;
    }

    public LocalDateTime getDataConsulta() {
        return dataConsulta;
    }

    public void setDataConsulta(LocalDateTime dataConsulta) {
        this.dataConsulta = dataConsulta;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Disponibilidade that = (Disponibilidade) o;
        return id.equals(that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Disponibilidade{" +
                "id=" + id +
                ", uf='" + uf + '\'' +
                ", statusAutorizacao=" + statusAutorizacao +
                ", statusRetornoAutorizacao=" + statusRetornoAutorizacao +
                ", statusInutilizacao=" + statusInutilizacao +
                ", statusConsultaProtocolo=" + statusConsultaProtocolo +
                ", statusStatusServico=" + statusStatusServico +
                ", statusConsultaCadastro=" + statusConsultaCadastro +
                ", statusRecepcaoEvento=" + statusRecepcaoEvento +
                ", versaoWebService='" + versaoWebService + '\'' +
                ", dataConsulta=" + dataConsulta +
                '}';
    }
}
