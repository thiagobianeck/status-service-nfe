package br.com.bianeck.statusservicenf.services;

import br.com.bianeck.statusservicenf.model.Disponibilidade;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class JSoupService {

    @Value("${jsoup.paginaweb}")
    private String paginaWeb;

    public List<Disponibilidade> getDisponibilidades() throws IOException {
        List<Disponibilidade> disponibilidades = new ArrayList<>();
        Document doc = Jsoup.connect(paginaWeb).get();
        Elements tabelaWeb3 = doc.select("#ctl00_ContentPlaceHolder1_gdvDisponibilidade tbody tr:nth-child(n+2)");
        Elements tabelaWeb4 = doc.select("#ctl00_ContentPlaceHolder1_gdvDisponibilidade2 tbody tr:nth-child(n+2)");
        List<Disponibilidade> disponibilidadesWeb3 = getDisponiblidadesWeb(tabelaWeb3, "3.10");
        List<Disponibilidade> disponibilidadesWeb4 = getDisponiblidadesWeb(tabelaWeb4, "4.00");
        disponibilidades.addAll(disponibilidadesWeb3);
        disponibilidades.addAll(disponibilidadesWeb4);
        return disponibilidades;
    }

    private List<Disponibilidade> getDisponiblidadesWeb(Elements tabela, String versao) {
        List<Disponibilidade> disponibilidades = new ArrayList<>();
        tabela.forEach(element -> {
            Elements children = element.children();
            Disponibilidade disponibilidade = montaDisponibilidade(children);
            disponibilidade.setVersaoWebService(versao);
            disponibilidades.add(disponibilidade);
        });
        return disponibilidades;
    }

    private Disponibilidade montaDisponibilidade(Elements children) {
        Disponibilidade disponibilidade = new Disponibilidade();
        disponibilidade.setDataConsulta(LocalDateTime.now());
        disponibilidade.setUf(children.get(0).text());
        disponibilidade.setStatusAutorizacao(children.get(1).child(0).attr("src").contains("verde"));
        disponibilidade.setStatusRetornoAutorizacao(children.get(2).child(0).attr("src").contains("verde"));
        disponibilidade.setStatusInutilizacao(children.get(3).child(0).attr("src").contains("verde"));
        disponibilidade.setStatusConsultaProtocolo(children.get(4).child(0).attr("src").contains("verde"));
        disponibilidade.setStatusStatusServico(children.get(5).child(0).attr("src").contains("verde"));
        disponibilidade.setStatusConsultaCadastro(children.get(7).child(0).attr("src").contains("verde"));
        disponibilidade.setStatusRecepcaoEvento(children.get(8).child(0).attr("src").contains("verde"));
        return disponibilidade;
    }
}
