package br.com.bianeck.statusservicenf.config.security;

import br.com.bianeck.statusservicenf.model.Disponibilidade;
import br.com.bianeck.statusservicenf.repository.DisponibilidadeRepository;
import br.com.bianeck.statusservicenf.services.JSoupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class ScheduledTask {

    @Autowired
    private DisponibilidadeRepository disponibilidadeRepository;

    @Autowired
    private JSoupService JSoupService;

    @Scheduled(fixedRate = 10000)
    public void gravaRegistros() throws IOException {
        List<Disponibilidade> disponibilidades = JSoupService.getDisponibilidades();
        disponibilidadeRepository.saveAll(disponibilidades);
    }
}
