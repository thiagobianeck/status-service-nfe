package br.com.bianeck.statusservicenf.controller.dto;

import br.com.bianeck.statusservicenf.model.Disponibilidade;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.data.domain.Page;

import java.time.LocalDateTime;

public class DisponibilidadeDto {

    private Long id;
    private String uf;
    private boolean statusAutorizacao;
    private boolean statusRetornoAutorizacao;
    private boolean statusInutilizacao;
    private boolean statusConsultaProtocolo;
    private boolean statusStatusServico;
    private boolean statusConsultaCadastro;
    private boolean statusRecepcaoEvento;
    private String versaoWebService;
    private LocalDateTime dataConsulta;

    public DisponibilidadeDto(Disponibilidade disponibilidade) {
        this.id = disponibilidade.getId();
        this.uf = disponibilidade.getUf();
        this.statusAutorizacao = disponibilidade.isStatusAutorizacao();
        this.statusRetornoAutorizacao = disponibilidade.isStatusRetornoAutorizacao();
        this.statusInutilizacao = disponibilidade.isStatusInutilizacao();
        this.statusConsultaProtocolo = disponibilidade.isStatusConsultaProtocolo();
        this.statusStatusServico = disponibilidade.isStatusStatusServico();
        this.statusConsultaCadastro = disponibilidade.isStatusConsultaCadastro();
        this.statusRecepcaoEvento = disponibilidade.isStatusRecepcaoEvento();
        this.versaoWebService = disponibilidade.getVersaoWebService();
        this.dataConsulta = disponibilidade.getDataConsulta();
    }

    public Long getId() {
        return id;
    }

    public String getUf() {
        return uf;
    }

    public boolean isStatusAutorizacao() {
        return statusAutorizacao;
    }

    public boolean isStatusRetornoAutorizacao() {
        return statusRetornoAutorizacao;
    }

    public boolean isStatusInutilizacao() {
        return statusInutilizacao;
    }

    public boolean isStatusConsultaProtocolo() {
        return statusConsultaProtocolo;
    }

    public boolean isStatusStatusServico() {
        return statusStatusServico;
    }

    public boolean isStatusConsultaCadastro() {
        return statusConsultaCadastro;
    }

    public boolean isStatusRecepcaoEvento() {
        return statusRecepcaoEvento;
    }

    public String getVersaoWebService() {
        return versaoWebService;
    }

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy HH:mm:ss", locale = "pt_BR")
    public LocalDateTime getDataConsulta() {
        return dataConsulta;
    }

    public static Page<DisponibilidadeDto> converter(Page<Disponibilidade> disponibilidades) {
        return disponibilidades.map(DisponibilidadeDto::new);
    }
}
