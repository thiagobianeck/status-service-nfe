package br.com.bianeck.statusservicenf.controller.dto;

import java.util.List;
import java.util.Map;

public class CountDisponibilidadeDto {

    public static final String UF = "uf";
    public static final String VERSAO_WEB_SERVICE = "versao";
    public static final String DISPONIBILIDADES = "disponibilidades";
    public static final String INDISPONIBILIDADES = "indisponibilidades";

    private String uf;
    private String versaoWebService;
    private long disponibilidades;
    private long indisponibilidades;

    public CountDisponibilidadeDto() {}

    public CountDisponibilidadeDto(Map<String, Object> values) {
        this.uf = values.get(UF) != null ? (String) values.get(UF): null;
        this.versaoWebService = values.get(VERSAO_WEB_SERVICE) != null ? (String) values.get(VERSAO_WEB_SERVICE): null;
        this.disponibilidades = values.get(DISPONIBILIDADES) != null ? (long) values.get(DISPONIBILIDADES): null;
        this.indisponibilidades = values.get(INDISPONIBILIDADES) != null ? (long) values.get(INDISPONIBILIDADES): null;
    }

    public String getUf() {
        return uf;
    }

    public String getVersaoWebService() {
        return versaoWebService;
    }

    public long getDisponibilidades() {
        return disponibilidades;
    }

    public long getIndisponibilidades() {
        return indisponibilidades;
    }

}
