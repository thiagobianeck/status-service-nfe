package br.com.bianeck.statusservicenf.controller;

import br.com.bianeck.statusservicenf.controller.dto.CountDisponibilidadeDto;
import br.com.bianeck.statusservicenf.controller.dto.DisponibilidadeDto;
import br.com.bianeck.statusservicenf.model.Disponibilidade;
import br.com.bianeck.statusservicenf.repository.DisponibilidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
@RequestMapping("/disponibilidades")
public class DisponibilidadesController {


    @Autowired
    private DisponibilidadeRepository disponibilidadeRepository;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");


    @GetMapping
    public Page<DisponibilidadeDto> lista (
            @RequestParam(required = false) String uf,
            @RequestParam(required = false) String dateFrom,
            @RequestParam(required = false) String dateTo,
            @PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 30) Pageable paginacao) {

        if (uf == null && dateFrom == null && dateTo == null) {
            Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findAll(paginacao);
            return DisponibilidadeDto.converter(disponibilidades);
        } else if (uf == null && dateFrom != null && dateTo != null) {
            LocalDateTime dateFromObj = LocalDateTime.parse(dateFrom, FORMATTER);
            LocalDateTime dateToObj = LocalDateTime.parse(dateTo, FORMATTER);
            Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findByDataConsultaBetween(dateFromObj, dateToObj, paginacao);
            return DisponibilidadeDto.converter(disponibilidades);
        } else if (uf != null && dateFrom != null && dateTo != null) {
            LocalDateTime dateFromObj = LocalDateTime.parse(dateFrom, FORMATTER);
            LocalDateTime dateToObj = LocalDateTime.parse(dateTo, FORMATTER);
            Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findByUfAndDataConsultaBetween(uf, dateFromObj, dateToObj, paginacao);
            return DisponibilidadeDto.converter(disponibilidades);
        } else {
            Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findByUf(uf, paginacao);
            return DisponibilidadeDto.converter(disponibilidades);
        }

    }

    @GetMapping("/{uf}")
    public Page<DisponibilidadeDto> getByUf(
            @PathVariable String uf,
            @PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 30) Pageable paginacao) {
        Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findByUf(uf, paginacao);
        return DisponibilidadeDto.converter(disponibilidades);
    }

    @GetMapping("/byVersion")
    public Page<DisponibilidadeDto> getByVersion(
            @RequestParam(required = true) String version,
            @PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 30) Pageable paginacao) {
        Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findByVersaoWebService(version, paginacao);
        return DisponibilidadeDto.converter(disponibilidades);
    }

    @GetMapping("/countDisponibilidades")
    public List<CountDisponibilidadeDto> getCountDisponibilidades(@RequestParam(required = false) String version) {

        if (version == null) {
            List<Map<String, Object>> results = disponibilidadeRepository.countDisponibilidade();
            return results.stream().map(CountDisponibilidadeDto::new).collect(Collectors.toList());
        }

        List<Map<String, Object>> results = disponibilidadeRepository.countDisponibilidadeByVersaoWebService(version);
        return results.stream().map(CountDisponibilidadeDto::new).collect(Collectors.toList());
    }

    @GetMapping("/byData")
    public Page<DisponibilidadeDto> getByData(
            @RequestParam(required = true) String dateFrom,
            @RequestParam(required = true) String dateTo,
            @PageableDefault(sort = "id", direction = Direction.ASC, page = 0, size = 30) Pageable paginacao) {
        LocalDateTime dateFromObj = LocalDateTime.parse(dateFrom, FORMATTER);
        LocalDateTime dateToObj = LocalDateTime.parse(dateTo, FORMATTER);
        Page<Disponibilidade> disponibilidades = disponibilidadeRepository.findByDataConsultaBetween(dateFromObj, dateToObj, paginacao);
        return DisponibilidadeDto.converter(disponibilidades);
    }

}
