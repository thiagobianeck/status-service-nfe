package br.com.bianeck.statusservicenf;

import br.com.bianeck.statusservicenf.model.Disponibilidade;
import br.com.bianeck.statusservicenf.services.JSoupService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class StatusservicenfApplicationTests {


	@Autowired
	private br.com.bianeck.statusservicenf.services.JSoupService JSoupService;

	@Test
	public void contextLoads() {

		try {
			List<Disponibilidade> disponibilidades = JSoupService.getDisponibilidades();

			disponibilidades.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
